#ifndef _C_MEM_H_
#define _C_MEM_H_

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

int c_strlen(char *cptr);
void c_strcpy(char *pptr, const char *sptr);
int c_strcmp(const char * ptr1, const char * ptr2);
void c_strcat(char *s, char *t);
void c_memcpy(void *dst, const void *src, unsigned int len);
void c_bzero(void *dst, unsigned int len);
void c_memset(void *dst, int c, unsigned int len);
void* c_memmove (void * dest, const void *src, size_t count);
int ctohex(char c);

#ifdef __cplusplus
}
#endif 

#endif
