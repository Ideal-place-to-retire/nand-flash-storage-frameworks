#ifndef STORE_CONF_H
#define STORE_CONF_H


#ifndef TRUE
#define TRUE    1
#endif
#ifndef FALSE
#define FALSE   0
#endif


#define CYCLE_STORE_BLOCK(N)     (N+1)  // cycle store total blocks

/***********宏配置***********************/ 

//内部flash配置
#define IN_FLASH_ONE_PAGE_SIZE      (1024*2)                  // 内部flash一页大小      
            
            
// 外部flash配置            
#define OUT_FLASH_ONE_PAGE_SIZE     (1024*2)                 // 外部flash一页大小  
#define OUT_FLASH_PAGES_PRE_BLOCK   64                       // 外部flash一块的页数
#define OUT_FLASH_BLOCK_SIZE        OUT_FLASH_ONE_PAGE_SIZE*OUT_FLASH_PAGES_PRE_BLOCK //0X20000                 // 外部flash一块的大小
            
            
//存储信息配置            
#define ONE_RECORD_SIZE             64                      //一条记录的字节大小   
#define IN_FLASH_BUFF_SIZE          (1024*2)                  // 内部flash存储分配的缓存区字节大小
#define IN_FLASH_BUFF_STORE_NUM     (IN_FLASH_BUFF_SIZE/ONE_RECORD_SIZE) // 内部缓冲区的存储条数
#define OUT_FLASH_BUFF_BLOCK_NUM    CYCLE_STORE_BLOCK(1)    // 外部flash为每个数据类型分配的存储块数 ，N+1块         






#define IN_ADDR_START                    0x08000000
#define IN_ADDR_OFFSET(offset_pages)     (IN_ADDR_START + offset_pages*IN_FLASH_ONE_PAGE_SIZE)

// 存储索引的存储地址
#define RECORD_INDEX_ADDR   IN_ADDR_OFFSET(72)

// 数据类型存储的起始地址
#define IN_ALARM_ADDR       IN_ADDR_OFFSET(73)
#define OUT_ALAARM_ADDR     (0x0000U)

#define IN_FIRST_FIRE_ADDR   IN_ADDR_OFFSET(74)
#define OUT_FIRST_FIRE_ADDR  (0X40000)

#define IN_FAULT_ADDR    IN_ADDR_OFFSET(75)
#define OUT_FAULT_ADDR    (0X80000)


#define IN_OTHER_ADDR    IN_ADDR_OFFSET(76)
#define OUT_OTHER_ADDR    (0XC0000)

/********************* end *************************/








#endif 