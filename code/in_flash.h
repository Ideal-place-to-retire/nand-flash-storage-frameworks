#ifndef IN_FLASH_H
#define IN_FLASH_H

#include "stdint.h"

uint8_t in_flash_erase(uint32_t addr);
uint8_t in_flash_read(uint32_t, uint8_t*, uint16_t);
uint8_t in_flash_write(uint32_t, uint8_t*, uint16_t);
void in_flash_print(uint32_t start_addr, uint32_t end_addr);


#endif