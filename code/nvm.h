#ifndef _NVM_H_
#define _NVM_H_

#include "stdint.h"
#include "store_conf.h"


// 在linux中调试使用
#define USE_IN_LINUX 		1


#define NVM0_INDEX 0
#define NVM0_ADDR(addr) ((uint32_t)addr+ ((uint32_t)NVM0_INDEX << 30))
#define NVM0_INDEX_BASE_ADDR (uint32_t)0

#define NVM1_INDEX 1
#define NVM1_ADDR(addr) ((uint32_t)addr + ((uint32_t)NVM1_INDEX << 30))//在地址前边加入序号1或2 ，在进行读写的时候，右移三十位得到序号1或者2
#define NVM1_INDEX_BASE_ADDR (uint32_t)0x40000000

#define NVM2_INDEX 2
#define NVM2_ADDR(addr) ((uint32_t)addr + ((uint32_t)NVM2_INDEX << 30))
#define NVM2_INDEX_BASE_ADDR (uint32_t)0x80000000





//////存储信息种类配置////////




typedef enum
{
//NVM0	IN_FLASH
	DIDEX_RECORD_INDEX  = 0,
	DIDEX_IN_ALARM_RECORD,
	DIDEX_IN_FIRST_FIRE_RECORD,
    DIDEX_IN_FAULT_RECORD,
	DIDEX_IN_OTHER_RECORD,

	
//NVM1  OUT_FLASH
    DIDEX_OUT_ALARM_RECORD,
	DIDEX_OUT_FIRST_FIRE_RECORD,
    DIDEX_OUT_FAULT_RECORD,
	DIDEX_OUT_OTHER_RECORD,

//NVM2
   
	
    DINDEX_MAX_NUM //DON'T DELETE
} ENU_DATA_INDEX;


///////////// end ////////////////////////////


typedef struct DATA_INDEX
{
    uint8_t ucDataIndex; //数据索引
    uint32_t uwAddr; //数据地址
    uint32_t uwLen;  //数据长度
} STR_DATA_INDEX; //数据索引结构体

typedef struct NVM_OPERATIONS
{ 
    void (*pfnInit)(void); //NVM初始化指针
	uint8_t (*pflash_erase)(uint32_t sect);//flash 擦
    uint8_t (*pfnRead)(uint32_t, uint8_t*, uint16_t);  //NVM读指针
    uint8_t (*pfnWrite)(uint32_t, uint8_t*, uint16_t); //NVM写指针      
} STR_NVM_OPERATIONS; //非易失存储器操作接口


typedef struct 
{ 
	uint8_t (*pNvmConfig)(void);
	uint8_t (*pNVMErase)(uint8_t ucIndex);
	uint8_t (*pNVMRead)(uint8_t ucIndex, void *ptr, uint16_t usLength);
	uint8_t (*pNVMWrite)(uint8_t ucIndex, void *ptr, uint16_t usLength);
	uint32_t (*pNVMGetLengthByIndex)(uint8_t ucIndex);
	uint32_t (*pNVMGetAddrByIndex)(uint8_t ucIndex);
	uint8_t (*pNVMEraseByOffset)(uint8_t ucIndex, uint32_t uwOffset);
	uint8_t (*pNVMReadByOffset)(uint8_t ucIndex, uint32_t uwOffset, void *ptr, uint16_t usLength);
	uint8_t (*pNVMWriteByOffset)(uint8_t ucIndex, uint32_t uwOffset, void *ptr, uint16_t usLength);
	#if USE_IN_LINUX
	uint8_t (*pNVM_flash_print)(uint8_t ucIndex, uint32_t uwOffset, uint16_t usLength);
	#endif
} NVM_INTERFACE; //NVM文件的函数操作接口




uint8_t NvmConfig(void);
uint8_t NVMErase(uint8_t ucIndex);
uint8_t NVMRead(uint8_t ucIndex, void *ptr, uint16_t usLength);
uint8_t NVMWrite(uint8_t ucIndex, void *ptr, uint16_t usLength);
uint32_t NVMGetLengthByIndex(uint8_t ucIndex);
uint32_t NVMGetAddrByIndex(uint8_t ucIndex);
uint8_t NVMEraseByOffset(uint8_t ucIndex, uint32_t uwOffset);
uint8_t NVMReadByOffset(uint8_t ucIndex, uint32_t uwOffset, void *ptr, uint16_t usLength);
uint8_t NVMWriteByOffset(uint8_t ucIndex, uint32_t uwOffset, void *ptr, uint16_t usLength);



extern NVM_INTERFACE StrNvmOperaTable;

#endif /* _NVM_H_*/


