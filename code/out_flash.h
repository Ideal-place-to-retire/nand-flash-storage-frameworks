#ifndef OUT_FLASH_H
#define OUT_FLASH_H

#include "stdint.h"

uint8_t out_flash_erase(uint32_t addr);
uint8_t out_flash_read(uint32_t, uint8_t*, uint16_t);
uint8_t out_flash_write(uint32_t, uint8_t*, uint16_t);
void out_flash_print(uint32_t start_addr, uint32_t end_addr);


#endif