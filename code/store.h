#ifndef STORE_H
#define STORE_H

#include "stdint.h"


#define FIRST_ALALRM_CODE           2
#define FIRE_ALARM_CODE             3
#define POWER_FAULT_CODE_LOW        49
#define POWER_FAULT_CODE_HIGH       56
#define FAULT_CODE_LOW              80
#define FAULT_CODE_HIGHT            90



// 数据信息格式  data information formats
#pragma pack (1)  // must one Byte alignment
typedef struct 
{
    uint16_t            controllerr_num;       
    uint8_t             unity_num;
    uint8_t             device_num;
    uint8_t             channel_num;
    uint16_t            device_type;               /* the type of device */ 
    uint16_t            event_type;                /* the type of event */ 
    uint16_t            statue_type;               /* the type of statue for instance */  
    uint8_t             year;
    uint8_t             month;
    uint8_t             day;
    uint8_t             hours;
    uint8_t             minute;
    uint8_t             second;

}STR_MESSAGE_SPECIAL;
#pragma pack ()

// 数据存储和导出格式  Data export and data storage formats
#pragma pack (1)     // must one Byte alignment
typedef struct 
{
    uint8_t             head_start;                /* stable value :0X40 */ 
    uint32_t            record_total_num;          /* use the lower 3 bytes */ 
    uint8_t             ctrl_addr;                 /* the address of fire alarm controller */ 
    uint16_t            controller_type;           /* the type of controller  */ 
    uint8_t             product_code[20];          /* the number of product */ 
    STR_MESSAGE_SPECIAL data_infor;                /* specitic information */  
    uint16_t            CRC;
    uint8_t             stop_end;                  /* stable value 0X40 */ 
}STR_MESSAGE_FORMAT;
#pragma pack ()

typedef enum
{
    read_data        = 1,       /* 数据读 */ 
    retrans_data     = 2,       /* 数据重发 */ 
    read_first_fire  = 3,       /* 数据读首警 */ 
    read_alarm       = 4,       /* 数据读火警信息 */ 
                
}ENU_COMMOND_TYPE;


// 请求数据的命令格式
typedef struct 
{
    uint8_t             head;                     /* fixed 0X40 */ 
    uint8_t             identify_code;            /* Identification code */ 
    uint8_t             version; 
    uint8_t             export_addr;              /* 固定0X7E， fixed value 0x7E ,The address of the special tool for data export,  */ 
    uint8_t             export_code;              /* 固定0X7F， fixed value 0x7F */ 
    uint8_t             commond_len;              /* the length of commond */  
    ENU_COMMOND_TYPE    commond_data;             /* type of command */ 
    uint16_t            CRC;
    uint8_t             stop_end;                 /* fixed 0X40 */ 

}STR_EXPORT_COMMAND;




// 存储信息的种类，用于建立索引数组
typedef enum
{
    ENU_FIRST_FIRE = 0,
    ENU_ALARM,
    ENU_FAULT,
    ENU_OTHER,
    ENU_MESSAGE_TYPE_MAX
    
}ENU_MESSAGE_TYPE;



/**
 * @brief 存储索引初始化
 * 
 */
void store_index_init(void);

/**
 * @brief 存储函数，将记录存到内部flash缓冲区，
 *          函数内部自动计算是否需要向外部flash移动。
 * 
 * @param infor 
 */
void store_new_message_to_in_flash(STR_MESSAGE_FORMAT * infor);

/**
 * @brief Get the type record total num object
 *         获得某个类型的记录 存储数量
 * @param message_type 
 * @return uint32_t 
 */
uint32_t get_type_record_total_num(ENU_MESSAGE_TYPE message_type);


/**
 * @brief Get the type record total num object
 *         获得所有类型的记录 存储数量
 * @param message_type 
 * @return uint32_t 
 */
uint32_t get_all_record_number(void);

/**
 * @brief 读取索引初始化
 * 
 */
void read_index_init(void);

/**
 * @brief 设置要读取的记录类型
 * 在同一类型未读取结束时，再发该类型，再次请求的命令会被忽略。
 * 最好不要在 读取过程中，再次发送其他请求命令。
 * 
 * @param message_type 
 */
void read_ctrl_message_add(ENU_MESSAGE_TYPE message_type);

/**
 * @brief 将设置的读取的记录类型 以不阻塞的方式进行读出
 *      读出记录类型的顺序 按照 命令下发的类型顺序进行。
 * 
 */
void read_ctrl_schdule(void);


/**
 * @brief 清除指定类型的记录
 * 
 */
void clear_record(ENU_MESSAGE_TYPE message_type);





#endif



